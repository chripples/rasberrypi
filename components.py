import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

class Led:
    def __init__(self, port):
        self._mode = False
        self._port = port
        GPIO.setup(port, GPIO.OUT)
    
    def toggle(self):
        self._mode = not self._mode
        if self._mode:
            GPIO.output(self._port, GPIO.HIGH)
        else:
            GPIO.output(self._port, GPIO.LOW)

    def on(self):
        self._mode = True
        GPIO.output(self._port, GPIO.HIGH)

    def off(self):
        self._mode = False
        GPIO.output(self._port, GPIO.LOW)

class Switch:
    def __init__(self, port):
        GPIO.setup(port, GPIO.IN)
        self._port = port

    def check(self):
        return GPIO.input(self._port) == 1