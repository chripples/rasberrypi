import RPi.GPIO as GPIO
import time
import components


led = components.Led(18)
switch = components.Switch(26)

def Loop():
    if switch.check():
        time.sleep(1)
        led.toggle()
    else:
        led.off()

while True:
    Loop()